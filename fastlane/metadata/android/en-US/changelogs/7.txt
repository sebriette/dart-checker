added:
- display elapsed time at end of match

fixed:
- bug in match mode: "Undo" changed order of player, if no dart was entered yet
- missing english translation in cricket mode
