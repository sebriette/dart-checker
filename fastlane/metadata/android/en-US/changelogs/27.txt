added:
- X01 / SET/LEG / FREE / ELIMINATION: list of checkout suggestions for singleout, doubleout or masterout (just touch the suggestion)
- PREFERENCES: selection of favorite checkout segment for checkout suggestion

modified:
- CRICKET / HALVE IT: bigger textsize of score(s)

fixed:
- HALVE IT: the last possible split in game was not saved
